import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

// https://github.com/manfredsteyer/angular-oauth2-oidc

export class AuthConfig {
  /**
   * Oauth Endpoint URL
   */
  auth_url = "";
  client_id = "";
  response_type = "token";
  redirect_uri = "";
}

@Injectable({
  providedIn: "root"
})
export class AuthService {
  token: string | null = null;

  constructor(private config: AuthConfig) {
    const jsonToken = sessionStorage.getItem("token");
    if (jsonToken) {
      this.token = JSON.parse(jsonToken);
    }
    if (location.hash) {
      const p = new HttpParams({
        fromString: location.hash
      });

      this.token = p.get("#access_token") || this.token;
      sessionStorage.setItem("token", JSON.stringify(this.token));
      location.hash = "";
    }
  }
  authorize() {
    const { auth_url, redirect_uri, response_type, client_id } = this.config;

    const p = new HttpParams({
      fromObject: {
        redirect_uri,
        response_type,
        client_id
      }
    });
    location.href = `${auth_url}?${p.toString()}`;
  }

  getToken() {
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}
