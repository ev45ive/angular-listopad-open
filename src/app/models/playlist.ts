export interface Playlist {
  id: number;
  name: string;
  favorite: boolean;
  color: string;
  tracks?: Track[];
}

export interface Track {
  id: number;
  name: string;
}


// type Entity = Playlist | Track;

// const e: Entity = {};

// switch (e.type) {
//   case "Track":
//     e.id = 123;
//     break;
//   case "Playlist":
//     e.favorite = true;
// }

// if(e instanceof Playlist){
//   e
// }

// if(typeof p.id == 'number'){
//   p.id // number
// }

// interface Point {
//   x: number;
//   y: number;
// }

// interface Vector {
//   x: number;
//   y: number;
// }
// const p1: Point = { x: 12, y: 23 };

// const v1: Vector = p1

// function setup(options){
//   const  defaults = null;

//   if(options == undefined){
//     options = defaults
//   }
// }
// setup()
