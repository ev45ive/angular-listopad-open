import {
  async,
  ComponentFixture,
  TestBed,
  inject
} from "@angular/core/testing";

import { ILoveTestingComponent } from "./i-love-testing.component";
import { By } from "@angular/platform-browser";
import { Observable, EMPTY } from "rxjs";
import { EventEmitter } from "@angular/core";

fdescribe("ILoveTestingComponent", () => {
  let component: ILoveTestingComponent;
  let fixture: ComponentFixture<ILoveTestingComponent>;
  let service: jasmine.SpyObj<{
    sendRequest(): void;
    getMessages(): Observable<string>;
  }>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ILoveTestingComponent],
      imports: [],
      providers: [
        {
          provide: "APISERVIS",
          useFactory() {
            return jasmine.createSpyObj("ApiServis", [
              "sendRequest",
              "getMessages"
            ]);
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    TestBed.get("APISERVIS").getMessages.and.returnValue(EMPTY);
    fixture = TestBed.createComponent(ILoveTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });

  it("renders message", () => {
    component.message = "Lubie placki malinowe";

    fixture.detectChanges();

    expect(
      fixture.debugElement.query(By.css("p")).nativeElement.innerHTML
    ).toMatch("Lubie placki malinowe");
  });

  it("updated message on user input", () => {
    const input = fixture.debugElement.query(By.css("input"));
    input.nativeElement.value = "placki";

    // input.nativeElement.dispatchEvent(new Event("input"));

    input.triggerEventHandler("input", {
      target: input.nativeElement
    });

    expect(component.message).toEqual("placki");
  });

  it('sends message "send" is clicked', () => {
    const button = fixture.debugElement.query(By.css("input[type=button]"));

    const spy = spyOn(component, "send");

    button.nativeElement.dispatchEvent(new MouseEvent("click"));

    expect(spy).toHaveBeenCalled();
  });

  it("shoud send request to service", inject(
    ["APISERVIS"],
    (apiservice: { sendRequest(): void }) => {
      component.message = "Placki!!!";
      component.send();
      expect(apiservice.sendRequest).toHaveBeenCalledWith("Placki!!!");
    }
  ));

  it("should render async message from service", inject(
    ["APISERVIS"],
    (
      apiservice: jasmine.SpyObj<{
        sendRequest(): void;
        getMessages(): Observable<string>;
      }>
    ) => {
      const emitter = new EventEmitter();
      apiservice.getMessages.and.returnValue(emitter);
      component.ngOnInit();
      emitter.next("placki?");
      expect(component.message).toEqual("placki?");
    }
  ));
});
