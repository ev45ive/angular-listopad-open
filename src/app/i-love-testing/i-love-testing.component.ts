import { Component, OnInit, Inject } from "@angular/core";
import { Observable } from "rxjs";

@Component({
  selector: "app-i-love-testing",
  templateUrl: "./i-love-testing.component.html",
  styleUrls: ["./i-love-testing.component.scss"]
})
export class ILoveTestingComponent implements OnInit {
  send() {
    this.service.sendRequest(this.message);
  }

  message = "";

  constructor(
    @Inject("APISERVIS")
    private service: {
      sendRequest(msg: string): void;
      getMessages(): Observable<string>;
    }
  ) {}

  ngOnInit() {
    this.service.getMessages().subscribe(msg => {
      this.message = msg;
    });
  }
}
