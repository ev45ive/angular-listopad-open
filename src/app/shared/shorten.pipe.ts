import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "shorten"
  // pure: false
})
export class ShortenPipe implements PipeTransform {
  // constructor(http){}

  transform(value: any, length = 5, dots = "..."): any {
    return value.substr(0, length) + dots;
    // return Math.random()
  }
}
