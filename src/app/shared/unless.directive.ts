import {
  Directive,
  TemplateRef,
  ViewContainerRef,
  Input,
  ComponentFactoryResolver,
  ViewRef
} from "@angular/core";
import { PlaylistsViewComponent } from "../playlists/views/playlists-view/playlists-view.component";

type UnlessContext = {
  $implicit: string;
};

@Directive({
  selector: "[appUnless]"
})
export class UnlessDirective {
  cache: ViewRef | null = null;

  @Input()
  set appUnless(hide: boolean) {
    if (hide) {
      // this.vcr.remove(0)
      this.cache = this.vcr.detach(0);
    } else {
      if (this.cache) {
        this.vcr.insert(this.cache);
      } else {
        // this.vcr.createEmbeddedView(this.tpl, {
        //   $implicit: "malinowe"
        // });

        const f = this.cfr.resolveComponentFactory(PlaylistsViewComponent);

        const cr = this.vcr.createComponent(f, 0);
        // cr.instance.playlists.push({
        //   id: 345234,
        //   favorite: false,
        //   name: "placki",
        //   color: "red"
        // });
      }
    }
  }

  constructor(
    private tpl: TemplateRef<UnlessContext>,
    private vcr: ViewContainerRef,
    private cfr: ComponentFactoryResolver
  ) {
    console.log("unless");

    // this.vcr.createEmbeddedView(this.tpl,{
    //   $implicit:'czekoladowe'
    // },1)

    // this.vcr.createEmbeddedView(this.tpl,{
    //   $implicit:'dobre'
    // },2)
  }
}
