import { Component, OnInit, Input, Inject, Optional, EventEmitter } from "@angular/core";
import { TabsComponent } from "../tabs/tabs.component";

@Component({
  selector: "app-tab",
  templateUrl: "./tab.component.html",
  styleUrls: ["./tab.component.scss"]
})
export class TabComponent implements OnInit {
  @Input() title = "";

  open = false;

  openChange = new EventEmitter()

  toggle() {
    this.openChange.emit()
  }

  constructor() {
  }

  ngOnInit() {}
}
