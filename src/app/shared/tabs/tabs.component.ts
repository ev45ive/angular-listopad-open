import { Component, OnInit, ViewChild, ElementRef, ContentChild, ContentChildren, QueryList } from "@angular/core";
import { TabComponent } from '../tab/tab.component';
import { TabsNavComponent } from "../tabs-nav/tabs-nav.component";

@Component({
  selector: "app-tabs",
  templateUrl: "./tabs.component.html",
  styleUrls: ["./tabs.component.scss"]
})
export class TabsComponent implements OnInit {

  @ContentChild(TabsNavComponent, {
    static: true
  })
  navigation!: TabsNavComponent;

  @ContentChildren(TabComponent,{
    // descendants:true
  })
  tabs = new QueryList<TabComponent>()
  
  toggle(active: TabComponent) {
    this.tabs.forEach(tab => {
      tab.open = tab === active;
    });
  }
  
  constructor() {}

  ngAfterContentInit(){
    this.navigation.tabs = this.tabs.toArray()

    this.tabs.forEach(tab => {
      tab.openChange.subscribe(()=>{
        this.toggle(tab)
      })
    })
  }

  ngOnInit() {
  }
}
