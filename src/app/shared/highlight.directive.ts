import {
  Directive,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  DoCheck,
  AfterViewInit,
  AfterContentChecked,
  AfterViewChecked,
  Renderer2,
  HostBinding,
  HostListener
} from "@angular/core";
import { inject } from "@angular/core/testing";

@Directive({
  selector: "[appHighlight]"
  // host: {
  //   "[style.border-color]": "hover? color: ''",
  //   "[style.color]": "hover? color: ''",
  //   "(mouseenter)": " hover = true ",
  //   "(mouseleave)": " hover = false "
  // }
})
export class HighlightDirective implements OnInit, OnChanges {
  //
  @Input("appHighlight")
  color = "";

  @HostBinding("style.color")
  @HostBinding("style.border-color")
  get activeColor() {
    return this.hover ? this.color : "";
  }

  // @HostBinding("class.active")
  hover = false;

  @HostListener("mouseenter", ["$event.x", "$event.y"])
  activate(x: number, y: number) {
    this.hover = true;
  }

  @HostListener("mouseleave")
  deactivate() {
    this.hover = false;
  }

  constructor(
    private elem: ElementRef<HTMLDivElement>,
    private renderer: Renderer2
  ) {
    // console.log("constructor"); //, elem, this.color);
  }

  ngOnInit() {
    // console.log("ngOnInit");
  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log("ngOnChanges");
  }

  // this.elem.nativeElement.style.color = this.color;

  // ngOnDestroy(){
  //     console.log("ngOnDestroy");
  // }

  // ngDoCheck() {
  //   console.log("ngDoCheck");
  // }

  // ngAfterViewInit() {
  //   console.log("AfterViewInit");
  // }

  // ngAfterViewChecked() {
  //   console.log("AfterViewChecked");
  // }
}

// this.unlisten1 = this.renderer.listen(this.elem.nativeElement,'mouseenter',()=>{
//   this.renderer.setStyle(this.elem.nativeElement, "color", this.color);
// })

// this.unlisten2 = this.renderer.listen(this.elem.nativeElement,'mouseleave',()=>{
//   this.renderer.setStyle(this.elem.nativeElement, "color",'');
// })

// console.log(HighlightDirective)
