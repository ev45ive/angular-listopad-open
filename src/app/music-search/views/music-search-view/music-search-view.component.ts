import {
  Component,
  OnInit,
  Inject,
  EventEmitter,
  SkipSelf
} from "@angular/core";
import { Album } from "../../../models/album";
import { MusicSearchService } from "../../services/music-search.service";
import { Subscription, Subject, ConnectableObservable } from "rxjs";
import {
  takeUntil,
  tap,
  multicast,
  refCount,
  share,
  shareReplay,
  map,
  filter
} from "rxjs/operators";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-music-search-view",
  templateUrl: "./music-search-view.component.html",
  styleUrls: ["./music-search-view.component.scss"],
  viewProviders: [
    // MusicSearchService,
    // {
    //   provide: TabComponent,
    //   useExisting: MusicSearchViewComponent
    // }
  ]
})
export class MusicSearchViewComponent implements OnInit {
  query$ = this.service.queryChanges;
  results$ = this.service.getAlbums().pipe(share());

  message = "";

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: MusicSearchService
  ) {
    this.route.queryParamMap
      .pipe(map(paramMap => paramMap.get("query")))
      .subscribe(query => query && this.service.search(query));
  }

  search(query: string) {
    this.router.navigate([], {
      queryParams: {
        query
      },
      replaceUrl: true
      // skipLocationChange:true
      // relativeTo: this.route
    });
  }

  ngOnInit() {}
}
