import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MusicSearchRoutingModule } from "./music-search-routing.module";
import { MusicSearchViewComponent } from "./views/music-search-view/music-search-view.component";
import { SearchFormComponent } from "./components/search-form/search-form.component";
import { SearchResultsComponent } from "./components/search-results/search-results.component";
import { AlbumCardComponent } from "./components/album-card/album-card.component";
import { environment } from "../../environments/environment";
import { MUSIC_API_URL } from "./services/tokens";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
// import { MusicSearchService } from "./services/music-search.service";
import { SharedModule } from "../shared/shared.module";
import { MusicSearchService } from "./services/music-search.service";

@NgModule({
  declarations: [
    MusicSearchViewComponent,
    SearchFormComponent,
    SearchResultsComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule,
    // HttpClientModule,
    // SecurityModule.forChild(),
    MusicSearchRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  // entryComponents:[MusicSearchViewComponent]
  // exports: [MusicSearchViewComponent],
  providers: [
    MusicSearchService,
    {
      provide: MUSIC_API_URL,
      useValue: environment.api_url
    }
    // {
    //   provide: MusicSearchService,
    //   useFactory(url: string, otherValue:any) {
    //     return new MusicSearchService(url);
    //   },
    //   deps: [MUSIC_API_URL, /* OtherToken */]
    // },
    // {
    //   provide: AbstractMusicSearchService,
    //   useClass: SpotifyMusicSearchService,
    //   // deps: [SPECIAL_API_URL]
    // },
    // {
    //   provide:MusicSearchService,
    //   useClass:MusicSearchService
    // },
    // MusicSearchService
  ]
})
export class MusicSearchModule {}
