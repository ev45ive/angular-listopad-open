import { Injectable, Inject, EventEmitter } from "@angular/core";
import { Album } from "src/app/models/album";
import { MUSIC_API_URL } from "./tokens";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { AuthService } from "../../security/auth.service";
import { AlbumsResponse } from "../../models/album";
import {
  Observable,
  from,
  EMPTY,
  throwError,
  of,
  concat,
  Subject,
  ReplaySubject,
  BehaviorSubject,
  AsyncSubject,
  forkJoin
} from "rxjs";
import {
  pluck,
  map,
  catchError,
  tap,
  startWith,
  switchMap,
  mergeAll,
  switchAll,
  mergeScan
} from "rxjs/operators";
import { MusicSearchModule } from '../music-search.module';

@Injectable({
  providedIn: "root" 
  // providedIn: MusicSearchModule
})
export class MusicSearchService {
  results: Album[] = [];

  constructor(
    @Inject(MUSIC_API_URL)
    private api_url: string,
    private http: HttpClient
  ) {
    (window as any).subject = this.albumChanges;

    // console.log('hello!')
    
    this.queryChanges
      .pipe(
        map(query => ({
          type: "album",
          q: query
        })),
        switchMap(params =>
          this.http.get<AlbumsResponse>(this.api_url, { params }).pipe(
            map(resp => resp.albums.items),
            catchError(err => {
              this.errorNotifications.next(err);
              return EMPTY;
            })
          )
        )
      )
      .subscribe(this.albumChanges);

    // forkJoin(A,B,C).pipe(map([a,b,c]=> a+b+c))
  }

  queryChanges = new BehaviorSubject<string>("batman");
  albumChanges = new BehaviorSubject<Album[]>(this.results);
  errorNotifications = new Subject<Error>();

  // Input
  search(query: string) {
    this.queryChanges.next(query);
  }

  // Output
  getAlbums() {
    return this.albumChanges;
  }

  ngOnDestroy(){
    console.log('bye bye!')
  }
}
