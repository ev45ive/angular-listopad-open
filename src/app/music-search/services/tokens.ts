import { InjectionToken } from "@angular/core";

export const MUSIC_API_URL = new InjectionToken<string>('Url for music search service');
