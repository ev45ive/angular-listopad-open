import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from "@angular/core";
import { Album } from "src/app/models/album";

@Component({
  selector: "app-search-results",
  templateUrl: "./search-results.component.html",
  styleUrls: ["./search-results.component.scss"],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchResultsComponent implements OnInit {
  
  // ngOnChanges() {
  //   this.cdr.detectChanges();
  // }

  startDate = new Date()
  amount = 12.99

  @Input()
  results: Album[] = [];

  constructor(private cdr: ChangeDetectorRef) {
    // cdr.detach();
  }

  ngOnInit() {}

  random() {
    return Math.random();
  }
}
