import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectionStrategy } from "@angular/core";
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
  AbstractControl,
  ValidationErrors,
  AsyncValidatorFn
} from "@angular/forms";
import {
  filter,
  distinctUntilChanged,
  debounceTime,
  mapTo,
  map,
  withLatestFrom
} from "rxjs/operators";
import { Observable, Observer, combineLatest, AsyncSubject } from "rxjs";
import { MusicSearchViewComponent } from "../../views/music-search-view/music-search-view.component";

@Component({
  selector: "app-search-form",
  templateUrl: "./search-form.component.html",
  styleUrls: ["./search-form.component.scss"],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchFormComponent implements OnInit {
  /* 
  Validator
   */
  censor: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const badword = "batman";

    const hasError =
      "string" === typeof control.value && control.value.includes(badword);

    return hasError
      ? {
          censor: { badword }
        }
      : null;
  };

  asyncCensor: AsyncValidatorFn = (
    control: AbstractControl
  ): Observable<ValidationErrors | null> => {
    // this.http.post('/validaion',value).pipe(map(res=>validation))

    return Observable.create((observer: Observer<ValidationErrors | null>) => {
      // console.log("Subscribe");
      // mySuperValidationLibrary.addValidationListener(observer.next)

      const handler = setTimeout(() => {
        const validationResult = this.censor(control);
        observer.next(validationResult);
        observer.complete();
      }, 1000);

      // teardown - on Unsubscribe
      return () => {
        // console.log("unsubscribe");
        clearTimeout(handler);
      };
    });
  };

  @Input()
  set query(q: string) {
    (this.queryForm.get("query") as FormControl)!.setValue(q,{
      emitEvent:false,
      // onlySelf:true
    });
  }

  queryForm = new FormGroup({
    query: new FormControl(
      "",
      [
        Validators.required,
        Validators.minLength(3)
        // this.censor
      ],
      [this.asyncCensor]
    ),
    type: new FormControl("album")
  });

  constructor(/* private parent:MusicSearchViewComponent */) {
    (window as any).form = this.queryForm;

    const field = this.queryForm.get("query");

    const validChanges = this.queryForm.statusChanges.pipe(
      filter(s => s === "VALID"),
      mapTo(true)
    );

    const valueChanges = field!.valueChanges.pipe(
      debounceTime(400),
      filter(q => q.length >= 3),
      distinctUntilChanged()
    );

    const searchChanges = validChanges.pipe(
      withLatestFrom(valueChanges),
      map(([valid, value]) => value)
    );

    searchChanges.subscribe(query => this.search(query));
  }

  ngOnInit() {}

  @Output()
  queryChange = new EventEmitter<string>();

  search(query: string) {
    this.queryChange.next(query);
  }
}
