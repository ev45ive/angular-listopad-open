import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlaylistsModule } from "./playlists/playlists.module";
import { SharedModule } from "./shared/shared.module";
import { SecurityModule } from "./security/security.module";
import { environment } from "../environments/environment";
import { MUSIC_API_URL } from './music-search/services/tokens';
import { HttpClientModule } from '@angular/common/http';
import { ILoveTestingComponent } from './i-love-testing/i-love-testing.component';

@NgModule({
  declarations: [AppComponent, ILoveTestingComponent],
  imports: [
    BrowserModule,
    PlaylistsModule,
    SharedModule,
    HttpClientModule,
    // MusicSearchModule,
    AppRoutingModule,
    SecurityModule.forRoot(environment.authConfig)
  ],
  providers: [
    // {
    //   provide: MUSIC_API_URL,
    //   useValue: environment.api_url
    // },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  // constructor(private app: ApplicationRef) {}
  // ngDoBootstrap() {
  //   setTimeout(() => {
  //     this.app.bootstrap(AppComponent, "app-root");
  //   }, 3000);
  // }
}
