import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { environment } from "../environments/environment";

const routes: Routes = [
  {
    path: "",
    redirectTo: "playlists",
    pathMatch: "full"
  },
  {
    path: "music",
    loadChildren: () =>
      import("./music-search/music-search.module").then(
        m => m.MusicSearchModule
      )
  },
  {
    path: "**",
    redirectTo: "playlists"
    // component: /* PageNotFOund  */ PlaylistsViewComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      enableTracing: environment.production === false
      // useHash: true
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
