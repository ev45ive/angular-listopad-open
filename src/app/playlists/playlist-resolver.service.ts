import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { Playlist } from "../models/playlist";
import { Observable } from "rxjs";
import { PlaylistsService } from "./services/playlists.service";

@Injectable({
  providedIn: "root"
})
export class PlaylistResolverService implements Resolve<Playlist | undefined> {
  
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Playlist | undefined> {
    const id = route.paramMap.get("playlist_id");

    return this.playlistsService.getOneById(parseInt(id!));
  }

  constructor(private playlistsService: PlaylistsService) {}
}
