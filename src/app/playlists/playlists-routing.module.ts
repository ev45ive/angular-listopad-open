import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PlaylistsViewComponent } from "./views/playlists-view/playlists-view.component";
import { SelectedPlaylistComponent } from "./views/selected-playlist/selected-playlist.component";
import { PlaylistEditorComponent } from "./views/playlist-editor/playlist-editor.component";
import { PlaylistResolverService } from "./playlist-resolver.service";

const routes: Routes = [
  {
    path: "playlists",
    // resolver:{
    //   playlists: PlaylistsResolver
    // },
    component: PlaylistsViewComponent,
    children: [
      {
        path: "",
        component: SelectedPlaylistComponent,
        data: {
          title: "No playlist selected"
        }
      },
      {
        path: ":playlist_id",
        resolve: {
          playlist: PlaylistResolverService
        },
        children: [
          {
            path: "",
            component: SelectedPlaylistComponent
          },
          {
            path: "edit",
            component: PlaylistEditorComponent
          },
          {
            path: "review",
            component: PlaylistEditorComponent
          }
        ]
      }
    ]
  }
  // {
  //   path: "playlists/:playlist_id",
  //   component: PlaylistsViewComponent
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule {}
