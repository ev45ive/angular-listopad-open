import { Injectable } from "@angular/core";
import { Playlist } from "src/app/models/playlist";
import { BehaviorSubject, Observable, of } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class PlaylistsService {
  private playlists = new BehaviorSubject<Playlist[]>([
    {
      id: 123,
      name: "Angular Hits",
      favorite: false,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "Angular Top20",
      favorite: false,
      color: "#ffff00"
    },
    {
      id: 345,
      name: "Best of Angular",
      favorite: false,
      color: "#00ffff"
    }
  ]);
  public playlistsChanges = this.playlists.asObservable();

  getAll() {
    // return this.http.get(/plalitsts)
  }

  getOneById(id: Playlist["id"]): Observable<Playlist | undefined> {
    // return this.http.get(/plalitsts/+id)
    return of(this.playlists.getValue().find(p => p.id == id));
  }

  updateOne(draft: Partial<Playlist>): Observable<Playlist | undefined> {
    // return this.http.post(/plalitsts, draft)

    const updatedPlaylsts = this.playlists
      .getValue()
      .map(p => (p.id == draft.id ? { ...p, ...draft } : p));

    this.playlists.next(updatedPlaylsts);

    return this.getOneById(draft.id!);
  }

  // save(draft: Playlist) {
  //   const index = this.playlists.findIndex(
  //     // match id
  //     p => p.id === draft.id
  //   );

  //   if (index !== -1) {
  //     this.playlists.splice(index, 1, draft);
  //     this.selected = draft;
  //     this.mode = "show";
  //   }
  // }

  constructor() {}
}
