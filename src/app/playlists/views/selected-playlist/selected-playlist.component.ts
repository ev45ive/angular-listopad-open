import { Component, OnInit } from "@angular/core";
import { combineLatest } from "rxjs";
import { map, filter, switchMap } from "rxjs/operators";
import { PlaylistsService } from "../../services/playlists.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Playlist } from "src/app/models/playlist";

@Component({
  selector: "app-selected-playlist",
  templateUrl: "./selected-playlist.component.html",
  styleUrls: ["./selected-playlist.component.scss"]
})
export class SelectedPlaylistComponent implements OnInit {
  //
  playlists$ = this.playlistsService.playlistsChanges;

  selected$ = this.route.data.pipe(
    map(data => data["playlist"] as Playlist | undefined)
  );

  constructor(
    private playlistsService: PlaylistsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  edit() {
    this.router.navigate(["edit"], {
      relativeTo: this.route
    });
  }

  ngOnInit() {}
}
