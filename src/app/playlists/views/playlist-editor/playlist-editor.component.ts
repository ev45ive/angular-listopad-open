import { Component, OnInit } from "@angular/core";
import { combineLatest } from "rxjs";
import { map, filter, switchMap } from "rxjs/operators";
import { PlaylistsService } from "../../services/playlists.service";
import { Router, ActivatedRoute } from "@angular/router";
import { Playlist } from "src/app/models/playlist";

@Component({
  selector: "app-playlist-editor",
  templateUrl: "./playlist-editor.component.html",
  styleUrls: ["./playlist-editor.component.scss"]
})
export class PlaylistEditorComponent implements OnInit {
  playlists$ = this.playlistsService.playlistsChanges;

  selected$ = combineLatest(this.playlists$, this.route.paramMap).pipe(
    map(([playlists, paramMap]) => paramMap),
    map(paramMap => paramMap.get("playlist_id")),
    filter(id => id !== null),
    switchMap(id => this.playlistsService.getOneById(parseInt(id!)))
  );

  constructor(
    private playlistsService: PlaylistsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  save(draft: Playlist) {
    this.playlistsService.updateOne(draft).subscribe(() => {
      this.router.navigate(["../"]);
    });
  }

  ngOnInit() {}
}
