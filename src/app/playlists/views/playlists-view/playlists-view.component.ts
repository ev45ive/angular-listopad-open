import { Component, OnInit } from "@angular/core";
import { Playlist } from "src/app/models/playlist";
import { ActivatedRoute, Router } from "@angular/router";
import { map, filter, switchMap } from "rxjs/operators";
import { PlaylistsService } from "../../services/playlists.service";
import { combineLatest } from "rxjs";

@Component({
  selector: "app-playlists-view",
  templateUrl: "./playlists-view.component.html",
  styleUrls: ["./playlists-view.component.scss"]
})
export class PlaylistsViewComponent implements OnInit {
  playlists$ = this.playlistsService.playlistsChanges;

  selected$ = combineLatest(
    this.playlists$,
    this.route.firstChild!.paramMap
  ).pipe(
    map(([playlists, paramMap]) => paramMap),
    map(paramMap => paramMap.get("playlist_id")),
    filter(id => id !== null),
    switchMap(id => this.playlistsService.getOneById(parseInt(id!)))
  );

  constructor(
    private playlistsService: PlaylistsService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  select(playlist: Playlist) {
    this.router.navigate(["/playlists", playlist.id]);
  }

  ngOnInit() {}
}
