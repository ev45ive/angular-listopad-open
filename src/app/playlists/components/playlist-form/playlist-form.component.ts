import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Playlist } from "src/app/models/playlist";

@Component({
  selector: "app-playlist-form",
  templateUrl: "./playlist-form.component.html",
  styleUrls: ["./playlist-form.component.scss"]
})
export class PlaylistFormComponent implements OnInit {
  @Input()
  playlist!: Playlist;

  @Output()
  cancel = new EventEmitter();

  @Output()
  save = new EventEmitter<Playlist>();

  onCancel() {
    this.cancel.emit();
  }

  // onSave(draft: Pick<Playlist,'id'|'favorite'|'color'>) {
  onSave(draft: Partial<Playlist>) {
    
    const playlist = {
      ...this.playlist,
      ...draft
    };


    this.save.emit(playlist);
  }

  constructor() {}

  ngOnInit() {}
}


// type Partial<T> = { 
//    [k in keyof T]?:  T[k]
// };


// type PartialPlaylist = { 
//   [k in 'id' | 'name' | 'color']: Playlist[k]
// };

// type PartialPlaylist = { 
//   name: Playlist["name"]; 
//   color: Playlist["color"]
// };
