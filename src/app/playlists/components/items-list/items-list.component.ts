import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter,
  Output
} from "@angular/core";
import { Playlist } from "src/app/models/playlist";
import { NgForOf, NgIf, NgForOfContext } from "@angular/common";

NgIf;
NgForOf;
NgForOfContext;

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.scss"],
  encapsulation: ViewEncapsulation.Emulated
  // inputs:[
  //   "playlists:items"
  // ],
  // outputs:[
  //   "placki:plackiEvent"
  // ]
})
export class ItemsListComponent implements OnInit {


  @Input("items")
  playlists: Playlist[] = [];

  @Input()
  selected: Playlist | null = null;

  @Output()
  selectedChange = new EventEmitter<Playlist>();

  select(p: Playlist) {
    this.selectedChange.emit(p);
  }

  constructor() {}

  ngOnInit() {}

  trackFn(item: Playlist, index: number) {
    return item.id;
  }
}
