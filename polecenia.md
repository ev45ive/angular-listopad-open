
git clone https://bitbucket.org/ev45ive/angular-listopad-open.git

https://tiny.pl/t42hj 

npm i -g @angular/cli

cd ..
ng new nazwa-katalogu-dla-projektu

cd nazwa-katalogu-dla-projektu
npm start

<!-- Extensions -->
Angular language service
Angular Snippets
angular2-inline
angulat2-switcher
Prettier

<!-- Playlists -->
ng g m playlists -m app --routing true 

ng g c playlists/views/playlists-view --export

ng g c playlists/components/items-list
ng g c playlists/components/list-item
ng g c playlists/components/playlist-details
ng g c playlists/components/playlist-form

<!-- Shared module -->
ng g m shared -m playlists 
<!-- Highlight -->
ng g d shared/highlight --export 
<!-- unless -->
ng g d shared/unless --export 
<!-- Card -->
ng g c shared/card --export
<!-- Tabs -->
ng g c shared/tabs --export
ng g c shared/tab --export
ng g c shared/tabs-nav --export

<!-- Music Search Module -->

ng g m music-search --routing -m app 

ng g c music-search/views/music-search-view --export 

ng g c music-search/components/search-form 
ng g c music-search/components/search-results
ng g c music-search/components/album-card

ng g i models/album

ng g s music-search/services/music-search 


ng g m security -m app
ng g s security/auth 


ng g s security/auth-interceptor 



